<?php
namespace Asif\DBAdapter;
include('DBInterface.php');
class DBAdapter implements \DBInterface
{
	private static $classInstance;
	private static $host;
	private static $user;
	private static $password;
	private static $database;
	private static $isPersistent;
	private $tableName;
	private $compiledSql;
	private $statement;
	private $whereValues;
	

	public function __construct($host,$user,$password,$database,$isPersistent=false)
	{
		self::$host=$host;
		self::$user=$user;
		self::$password=$password;
		self::$database=$database;
		self::$isPersistent=$isPersistent;
		
	}

	public static function getInstance()
	{
		if(!self::$classInstance)
		{
			self::$classInstance=new \PDO("mysql:host=".self::$host.";dbname=".self::$database,self::$user,self::$password,[
				\PDO::ATTR_PERSISTENT=>self::$isPersistent
			]);
		}

		return self::$classInstance;
	}

	public function setTable($tableName)
	{
		$this->tableName=$tableName;
		return $this;
	}
	public function insert(array $fieldsToBeFilled=[])
	{
		$fieldsName=implode(',',array_keys($fieldsToBeFilled));
		$fieldsValue=array_values($fieldsToBeFilled);
		$placeholders=implode(',',array_fill(0,count($fieldsToBeFilled),'?'));
		$compiledSql="INSERT INTO $this->tableName(".$fieldsName.") values(".$placeholders.")";
		$statement= self::$classInstance->prepare($compiledSql);
		$statement->execute($fieldsValue);
		return self::$classInstance->lastInsertId();
	}

	public function select($columns="*")
	{
		$this->compiledSql="SELECT $columns FROM $this->tableName";
		
		return $this;
	}

	public function where(array $whereClause=[])
	{
		$whereKeys=array_keys($whereClause);
		$totalWhereKeys=count($whereKeys);
		$this->compiledSql.=" WHERE ";
		foreach($whereKeys as $key=>$val)
		{
			if($totalWhereKeys>$key+1)
			{
				$this->compiledSql.=$val."=? AND ";
			}
			else
			{
				$this->compiledSql.=$val."=?";
			}
		}
		$this->whereValues.=implode(',',array_values($whereClause));
		return $this;
	}

	public function orWhere(array $whereClause=[])
	{
		$whereKeys=array_keys($whereClause);
		$totalWhereKeys=count($whereKeys);
		foreach($whereKeys as $key=>$val)
		{
			if($totalWhereKeys>$key+1)
			{
				$this->compiledSql.=" OR ".$val."=?";
			}
			else
			{
				$this->compiledSql.=" OR ".$val."=?";
			}
		}
		$this->whereValues.=','.implode(',',array_values($whereClause));
		return $this;
	}

	public function orderBy(array $orderByParams=[])
	{
		$this->compiledSql.=" order by ".implode(',',array_values($orderByParams));
		return $this;
	}

	public function fetch($fetchType=\PDO::FETCH_ASSOC)
	{
		$this->statement=self::$classInstance->prepare($this->compiledSql);
		$this->statement->execute(explode(',',$this->whereValues));
		return $this->statement->fetchAll($fetchType);
	}
}



$obj=new DBAdapter("localhost","root","","herp");
$obj::getInstance();
$obj->setTable('test');
// print_r($obj->setTable('test')->insert(['name'=>'sadaf','roll'=,>12]));
echo '<pre>';
print_r($obj->setTable('test')->select('*')->where(['id'=>2,'name'=>'sada'])->orWhere(['id'=>5])->orderBy(['id desc'])->fetch());
print_r($obj);
